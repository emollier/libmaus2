#! /bin/bash
SCRIPTDIR=`dirname "${BASH_SOURCE[0]}"`
pushd ${SCRIPTDIR}
SCRIPTDIR=`pwd`
popd

../src/testprotalign
RET=$?

echo "Exiting with code ${RET}"

exit ${RET}
