/*
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_MATH_BERNOULLI_HPP)
#define LIBMAUS2_MATH_BERNOULLI_HPP

#include <libmaus2/math/LGammaCache.hpp>

namespace libmaus2
{
	namespace math
	{
		struct Bernoulli : public libmaus2::math::LGammaCache
		{
			double const p;
			double const lp;
			double const lp1;

			Bernoulli(double const rp)
			: p(rp), lp(::std::log(p)), lp1(::std::log(1-p))
			{
			}

			double getBern(uint64_t const k, uint64_t const n)
			{
				// n!/(k! * (n-k)!) * p^k * (1-p) ^ (n-k)
				return
					::std::exp(
						getLGamma(n+1)
						-
						getLGamma(k+1)
						-
						getLGamma(n-k+1)
						+
						k * lp + (n-k) * lp1
					)
					;

			}
		};
	}
}
#endif
