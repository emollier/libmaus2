/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_MATH_LOGNORMALDISTRIBUTION_HPP)
#define LIBMAUS2_MATH_LOGNORMALDISTRIBUTION_HPP

#include <cmath>

namespace libmaus2
{
	namespace math
	{
		struct LogNormalDistribution
		{
			static double erf(double const x)
			{
				if ( x >= 0 )
					return ::std::erf(x);
				else
					return -erf(-x);
			}

			static double erfc(double const x)
			{
				return 1.0 - erf(x);
			}

			static double cumulative(double const x, double const log_mu, double const log_sigma)
			{
				return 0.5 * erfc(- ( ::log(x) - log_mu ) / (log_sigma * ::std::sqrt(2)));
			}

			static double density(
				double const x,
				double const log_mu,
				double const log_sigma
			)
			{
				return (1.0 / x) * 1.0 / (log_sigma * std::sqrt(2.0 * M_PI) ) * ::std::exp(-(::std::pow(::std::log(x-log_mu),2.0))/(2.0 * log_sigma * log_sigma));
			}
		};
	}
}
#endif
