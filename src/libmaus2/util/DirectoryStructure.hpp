/*
    libmaus2
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_UTIL_DIRECTORYSTRUCTURE_HPP)
#define LIBMAUS2_UTIL_DIRECTORYSTRUCTURE_HPP

#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>
#include <libmaus2/aio/SerialisedPeeker.hpp>
#include <libmaus2/aio/OutputStreamFactoryContainer.hpp>
#include <libmaus2/util/Base64.hpp>

namespace libmaus2
{
	namespace util
	{
		/**
		 * class for generating directory hierarchies via a Makefile not exceeding a given fan out
		 **/
		struct DirectoryStructure
		{
			private:
			std::string tmpfn;
			uint64_t mod;
			uint64_t n;
			std::string prefix;
			uint64_t c;
			bool ascii;

			struct DirectoryEntry
			{
				std::string prefix;
				std::vector < uint64_t > V;

				DirectoryEntry() {}
				DirectoryEntry(std::string const & rprefix, std::vector < uint64_t > const & rV) : prefix(rprefix), V(rV) {}

				std::string toString() const
				{
					std::ostringstream ostr;

					ostr << prefix;

					for ( uint64_t i = 0; i < V.size(); ++i )
						ostr << "/" << V[i];

					return ostr.str();
				}

				bool operator<(DirectoryEntry const & D) const
				{
					if ( prefix != D.prefix )
						return prefix < D.prefix;

					if ( V.size() != D.V.size() )
						return V.size() < D.V.size();

					for ( uint64_t i = 0; i < V.size(); ++i )
						if ( V[i] != D.V[i] )
							return V[i] < D.V[i];

					return false;
				}

				std::ostream & serialise(std::ostream & out) const
				{
					libmaus2::util::StringSerialisation::serialiseString(out,prefix);
					libmaus2::util::NumberSerialisation::serialiseNumber(out,V.size());
					for ( uint64_t i = 0; i < V.size(); ++i )
						libmaus2::util::NumberSerialisation::serialiseNumber(out,V[i]);
					return out;
				}

				std::istream & deserialise(std::istream & in)
				{
					prefix = libmaus2::util::StringSerialisation::deserialiseString(in);
					V.resize(libmaus2::util::NumberSerialisation::deserialiseNumber(in));
					for ( uint64_t i = 0; i < V.size(); ++i )
						V[i] = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
					return in;
				}
			};

			struct DirectoryEntryRevComp
			{
				bool operator()(DirectoryEntry const & A, DirectoryEntry const & B) const
				{
					return B < A;
				}
			};


			std::vector<uint64_t> getVector(uint64_t const i) const
			{
				if ( ! (i<n) ) {
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] libmaus2::util::DirectoryStructure::getVector: i=" << i << " is out of range for n=" << n << std::endl;
					lme.finish();
					throw lme;
				}

				std::vector<uint64_t> V;

				uint64_t z = i;
				for ( uint64_t j = 0; j < c; ++j )
				{
					V.push_back(z%mod);
					z /= mod;
				}

				std::reverse(V.begin(),V.end());

				return V;
			}

			public:
			void setAsciiFlag(bool const rascii = false)
			{
				ascii = rascii;
			}

			/**
			 * get prefix for index i
			 **/
			std::string operator[](uint64_t const i) const
			{
				if ( ! (i<n) ) {
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] libmaus2::util::DirectoryStructure::operator[]: i=" << i << " is out of range for n=" << n << std::endl;
					lme.finish();
					throw lme;
				}

				std::vector<uint64_t> V = getVector(i);

				DirectoryEntry DS(prefix,V);
				DS.V.pop_back();

				std::ostringstream ostr;
				ostr << DS.toString() << "/" << V.back();

				return ostr.str();
			}

			/**
			 * get directory containing prefix for index i
			 **/
			std::string getDirectoryFor(uint64_t const i) const
			{
				if ( ! (i<n) ) {
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] libmaus2::util::DirectoryStructure::getDirectoryFor: i=" << i << " is out of range for n=" << n << std::endl;
					lme.finish();
					throw lme;
				}

				std::vector<uint64_t> V = getVector(i);

				DirectoryEntry DS(prefix,V);
				DS.V.pop_back();

				std::ostringstream ostr;
				ostr << DS.toString();

				return ostr.str();
			}

			/**
			 * print Makefile rules for generating directory hierarchy to out
			 *
			 * @param out stream for outputting commands
			 * @param dep dependency set for first layer of mkdir cascade
			 * @param batchsize use batches of this size when calling mkdir
			 * @return target designating that generation is complete
			 **/
			std::string printGenerate(std::ostream & out, std::string dep = std::string(), uint64_t const batchsize = 1) const
			{
				libmaus2::sorting::SerialisingSortingBufferedOutputFile<DirectoryEntry>::sortUnique(tmpfn);

				libmaus2::aio::SerialisedPeeker<DirectoryEntry> SP(tmpfn);
				DirectoryEntry D;
				while ( SP.peekNext(D) )
				{
					// length of vector
					uint64_t const l = D.V.size();

					uint64_t depcnt = 0;

					while ( SP.peekNext(D) && D.V.size() == l )
					{
						out << prefix << "_create_" << l << "_" << (depcnt++) << ": " << dep << "\n";

						std::ostringstream ddostr;
						uint64_t batchcnt = 0;
						for ( uint64_t j = 0; j < batchsize && SP.peekNext(D) && D.V.size() == l; ++j, ++batchcnt )
						{
							SP.getNext(D);
							std::string const s = D.toString();

							if ( ascii )
								out << "\tmkdir " << s << "\n";
							else
								libmaus2::util::StringSerialisation::serialiseString(ddostr,s);
						}

						if ( !ascii )
						{
							std::string const dd = libmaus2::util::Base64::encode(ddostr.str());
							out << "\thpcsched::mkdirbatch " << dd << "\n";
						}
					}

					std::ostringstream depstr;
					depstr << prefix << "_create_" << l;
					dep = depstr.str();

					out << dep << ":";
					for ( uint64_t i = 0; i < depcnt; ++i )
						out << " " << prefix << "_create_" << l << "_" << i;
					out << "\n";
					out << "\techo\n";
				}

				return dep;
			}

			/**
			 * create directories online
			 **/
			void doGenerate() const
			{
				libmaus2::sorting::SerialisingSortingBufferedOutputFile<DirectoryEntry>::sortUnique(tmpfn);

				libmaus2::aio::SerialisedPeeker<DirectoryEntry> SP(tmpfn);
				DirectoryEntry D;
				while ( SP.getNext(D) )
				{
					std::string const s = D.toString();
					libmaus2::aio::OutputStreamFactoryContainer::mkdir(s,0700 /* mode */);
				}
			}

			/**
			 * print Makefile rules for removing directory hierarchy to out
			 *
			 * @param out stream for outputting commands
			 * @param dep dependency set for first layer of rmdir cascade
			 * @param batchsize use batches of this size when calling rmdir
			 * @return target designating that removal is complete
			 **/
			std::string printRemove(std::ostream & out, std::string dep = std::string(), uint64_t const batchsize = 1) const
			{
				libmaus2::sorting::SerialisingSortingBufferedOutputFile<DirectoryEntry,DirectoryEntryRevComp>::sortUnique(tmpfn);

				libmaus2::aio::SerialisedPeeker<DirectoryEntry> SP(tmpfn);
				DirectoryEntry D;
				while ( SP.peekNext(D) )
				{
					// length of vector
					uint64_t const l = D.V.size();

					uint64_t depcnt = 0;

					while ( SP.peekNext(D) && D.V.size() == l )
					{
						out << prefix << "_remove_" << l << "_" << (depcnt++) << ": " << dep << "\n";

						std::ostringstream ddostr;
						uint64_t batchcnt = 0;
						for ( uint64_t j = 0; j < batchsize && SP.peekNext(D) && D.V.size() == l; ++j, ++batchcnt )
						{
							SP.getNext(D);
							std::string const s = D.toString();

							if ( ascii )
								out << "\trmdir " << s << "\n";
							else
								libmaus2::util::StringSerialisation::serialiseString(ddostr,s);
						}

						if ( ! ascii )
						{
							std::string const dd = libmaus2::util::Base64::encode(ddostr.str());

							out << "\thpcsched::rmdirbatch " << dd << "\n";
						}
					}

					std::ostringstream depstr;
					depstr << prefix << "_remove_" << l;
					dep = depstr.str();

					out << dep << ":";
					for ( uint64_t i = 0; i < depcnt; ++i )
						out << " " << prefix << "_remove_" << l << "_" << i;
					out << "\n";
					out << "\techo\n";
				}

				return dep;
			}

			/**
			 * delete directories online
			 **/
			void doRemove() const
			{
				libmaus2::sorting::SerialisingSortingBufferedOutputFile<DirectoryEntry,DirectoryEntryRevComp>::sortUnique(tmpfn);

				libmaus2::aio::SerialisedPeeker<DirectoryEntry> SP(tmpfn);
				DirectoryEntry D;
				while ( SP.getNext(D) )
				{
					std::string const s = D.toString();
					libmaus2::aio::OutputStreamFactoryContainer::rmdir(s);
				}
			}

			~DirectoryStructure()
			{
				libmaus2::aio::FileRemoval::removeFile(tmpfn);
			}

			/**
			 * constructor
			 *
			 * @param rtmpfn: temporary file for storing data
			 * @param rmod: maximum number of sub directories/files per directory
			 * @param rn: total number of file prefixes required
			 * @param rprefix: prefix for temporary directories
			 **/
			DirectoryStructure(std::string const & rtmpfn, uint64_t const rmod, uint64_t const rn, std::string const & rprefix)
			: tmpfn(rtmpfn), mod(rmod), n(rn), prefix(rprefix), c(1), ascii(false)
			{
				uint64_t t = rn;
				while ( t >= rmod )
				{
					t /= rmod;
					++c;
				}

				{
					libmaus2::aio::OutputStreamInstance OSI(tmpfn);

					for ( uint64_t i = 0; i < n; ++i )
					{
						std::vector<uint64_t> V = getVector(i);
						V.pop_back();

						DirectoryEntry D(prefix,V);
						while ( D.V.size() )
						{
							D.serialise(OSI);
							D.V.pop_back();
						}
						D.serialise(OSI);
					}

					OSI.flush();
				}


				#if 0
				for ( uint64_t i = 0; i < rn; ++i )
				{

					for ( uint64_t j = 0; j < V.size(); ++j )
						std::cerr << V[j] << ";";
					std::cerr << std::endl;
				}
				#endif
			}
		};
	}
}
#endif
