/*
    libmaus2
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/PathTools.hpp>
#include <filesystem>

std::string libmaus2::util::PathTools::getAbsPath(std::string const fn)
{
	if ( ! fn.size() )
		return fn;
	else
		return std::filesystem::absolute(std::filesystem::path(fn)).string();
}

std::string libmaus2::util::PathTools::getCurDir()
{
	return std::filesystem::current_path().string();
}

std::string libmaus2::util::PathTools::sbasename(std::string const & s)
{
	return std::filesystem::path(s).filename().string();
}

std::string libmaus2::util::PathTools::sdirname(std::string const & s)
{
	return std::filesystem::path(s).remove_filename().string();
}
