/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_CRAM_CRAMENCODINGWORKPACKAGEDATA_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_CRAM_CRAMENCODINGWORKPACKAGEDATA_HPP

#include <libmaus2/bambam/parallel/ScramCramEncoding.hpp>
#include <libmaus2/parallel/threadpool/ThreadWorkPackageData.hpp>
#include <atomic>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace cram
			{
				struct CramEncodingWorkPackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
				{
					typedef CramEncodingWorkPackageData this_type;
					typedef std::unique_ptr<this_type> unique_ptr_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					std::atomic<void *> workpackage;

					CramEncodingWorkPackageData() : libmaus2::parallel::threadpool::ThreadWorkPackageData(), workpackage(nullptr)
					{

					}

					void dispatch()
					{
						libmaus2::bambam::parallel::ScramCramEncoding::io_lib_cram_process_work_package(workpackage.load());
					}
				};
			}
		}
	}
}
#endif
