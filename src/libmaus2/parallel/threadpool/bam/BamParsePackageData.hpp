/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BAM_BAMPARSEPACKAGEDATA_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BAM_BAMPARSEPACKAGEDATA_HPP

#include <libmaus2/parallel/threadpool/bam/BamParsePackageDataHandler.hpp>
#include <libmaus2/parallel/threadpool/bgzf/BgzfBlockInfoEnqueDecompressHandler.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bam
			{
				struct BamParsePackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
				{
					typedef BamParsePackageData this_type;
					typedef std::unique_ptr<this_type> unique_ptr_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					// std::atomic<libmaus2::parallel::threadpool::input::ThreadPoolInputInfo *> inputinfo;
					libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::bgzf::BgzfBlockInfo> blockinfo;
					std::atomic<uint64_t> blockid;
					std::atomic<uint64_t> subid;
					std::atomic<uint64_t> absid;
					libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::bgzf::GenericBlock> ddata;
					std::atomic<uint64_t> n;
					std::atomic<libmaus2::parallel::threadpool::bgzf::BgzfBlockInfoEnqueDecompressHandler *> decompressHandler;
					std::atomic<BamParsePackageDataHandler *> handler;

					BamParsePackageData()
					: libmaus2::parallel::threadpool::ThreadWorkPackageData(),
					  // inputinfo(nullptr),
					  blockinfo(),
					  blockid(0),
					  subid(0),
					  absid(0),
					  ddata(),
					  n(0),
					  decompressHandler(nullptr),
					  handler(nullptr)
					{

					}

					void dispatch()
					{
						handler.load()->handle(*this);
					}
				};
			}
		}
	}
}
#endif
