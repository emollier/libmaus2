/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOLDISPATCHER_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOLDISPATCHER_HPP

#include <libmaus2/parallel/threadpool/ThreadWorkPackage.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			struct ThreadPool;

			struct ThreadPoolDispatchable
			{
				virtual ~ThreadPoolDispatchable() {}
				virtual void dispatch() = 0;
			};

			struct ThreadPoolGenericPackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
                        {
                        	libmaus2::util::atomic_shared_ptr<ThreadPoolDispatchable> P;

                        	ThreadPoolGenericPackageData() : libmaus2::parallel::threadpool::ThreadWorkPackageData() {}
                        	ThreadPoolGenericPackageData(libmaus2::util::shared_ptr<ThreadPoolDispatchable> r_P)
                        	: libmaus2::parallel::threadpool::ThreadWorkPackageData(), P(r_P) {}
                        };

			struct ThreadPoolDispatcher
			{
				typedef ThreadPoolDispatcher this_type;
				typedef std::unique_ptr<this_type> unique_ptr_type;
				typedef std::shared_ptr<this_type> shared_ptr_type;

				virtual ~ThreadPoolDispatcher() {}
				virtual void dispatch(libmaus2::util::shared_ptr<ThreadWorkPackage> package, ThreadPool * pool) = 0;
			};

			struct ThreadPoolGenericDispatcher : public ThreadPoolDispatcher
			{
				virtual void dispatch(libmaus2::util::shared_ptr<ThreadWorkPackage> package, [[maybe_unused]] ThreadPool * pool)
				{
					ThreadPoolGenericPackageData * data = dynamic_cast<ThreadPoolGenericPackageData *>(package->data.load().get());
					data->P.load()->dispatch();
				}
			};
		}
	}
}

#endif
