/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADWORKPACKAGE_HPP)
#define LIBMAUS2_PARALLEL_THREADWORKPACKAGE_HPP

#include <libmaus2/parallel/threadpool/ThreadWorkPackageData.hpp>
#include <atomic>
#include <libmaus2/util/atomic_shared_ptr.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			struct ThreadWorkPackage
			{
				typedef ThreadWorkPackage this_type;
				typedef std::unique_ptr<this_type> unique_ptr_type;
				typedef std::shared_ptr<this_type> shared_ptr_type;

				std::atomic<uint64_t> prio;
				std::pair< std::atomic<uint64_t>,std::atomic<uint64_t> > id;
				std::atomic < std::size_t > seq;
				libmaus2::util::atomic_shared_ptr<ThreadWorkPackageData> data;

				ThreadWorkPackage() : prio(0), id(0,0), seq(0), data() {}

				bool operator<(ThreadWorkPackage const & T) const
				{
					// process higher priority first
					if ( prio != T.prio )
						return prio > T.prio;
					// process lower id first
					else if ( id != T.id )
						return id < T.id;
					else
						return seq < T.seq;
				}

				ThreadWorkPackage & operator=(ThreadWorkPackage const & O) = delete;

				virtual ~ThreadWorkPackage() {}
			};
		}
	}
}
#endif
