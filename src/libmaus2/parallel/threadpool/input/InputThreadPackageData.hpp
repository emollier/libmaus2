/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADPACKAGEDATA_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADPACKAGEDATA_HPP

#include <libmaus2/parallel/threadpool/ThreadWorkPackageData.hpp>
#include <libmaus2/parallel/threadpool/input/InputThreadCallbackInterface.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace input
			{

				struct InputThreadPackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
				{
					typedef InputThreadPackageData this_type;
					typedef std::unique_ptr<this_type> unique_ptr_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					ThreadPoolInputInfo * inputinfo;
					InputThreadCallbackInterface * callback;

					InputThreadPackageData() : libmaus2::parallel::threadpool::ThreadWorkPackageData(), inputinfo(nullptr), callback(nullptr)
					{

					}
				};

			}
		}
	}
}
#endif
