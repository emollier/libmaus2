/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFBLOCKSUMMARY_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFBLOCKSUMMARY_HPP

#include <libmaus2/parallel/threadpool/input/ThreadPoolInputBlock.hpp>
#include <libmaus2/parallel/threadpool/input/ThreadPoolInputInfo.hpp>
#include <memory>
#include <libmaus2/util/atomic_shared_ptr.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				/**
				 * summary of BGZF blocks inside one input data block
				 **/
				struct BgzfBlockSummary
				{
					typedef BgzfBlockSummary this_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::input::ThreadPoolInputBlock> ptr;
					libmaus2::parallel::threadpool::input::ThreadPoolInputInfo * inputinfo;
					std::atomic<uint64_t> numblocks;
					std::atomic<uint64_t> finished;

					BgzfBlockSummary & operator=(BgzfBlockSummary const &) = delete;

					void reset(libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::input::ThreadPoolInputBlock> rptr, libmaus2::parallel::threadpool::input::ThreadPoolInputInfo * rinputinfo)
					{
						ptr = rptr;
						inputinfo = rinputinfo;
						numblocks.store(0);
						finished.store(0);
					}

					bool operator<(BgzfBlockSummary const & O) const
					{
						return ptr->blockid < O.ptr->blockid;
					}
				};
			}
		}
	}
}
#endif
