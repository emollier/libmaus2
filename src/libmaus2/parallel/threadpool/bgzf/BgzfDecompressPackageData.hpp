/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDECOMPRESSPACKAGEDATA_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDECOMPRESSPACKAGEDATA_HPP

#include <libmaus2/parallel/threadpool/bgzf/BgzfDataDecompressedInterface.hpp>
#include <libmaus2/parallel/threadpool/bgzf/ThreadPoolBGZFReadControl.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{

				struct BgzfDecompressPackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
				{
					typedef BgzfDecompressPackageData this_type;
					typedef std::unique_ptr<this_type> unique_ptr_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					std::atomic<libmaus2::parallel::threadpool::input::ThreadPoolInputInfo *> inputinfo;
					// callback to be run once data is decompressed
					std::atomic<BgzfDataDecompressedInterface *> callback;
					// block info of compressed block
					libmaus2::util::shared_ptr<BgzfBlockInfo> blockinfo;
					// uncompressed data block space
					libmaus2::util::shared_ptr<GenericBlock> data;
					// handler object used for decompressing data
					std::atomic<BgzfBlockInfoEnqueDecompressHandler *> decompHandler;

					BgzfDecompressPackageData()
					: libmaus2::parallel::threadpool::ThreadWorkPackageData(),
					  inputinfo(nullptr), callback(nullptr),
					  blockinfo(), data(),
					  decompHandler(nullptr)
					{

					}
				};
			}
		}
	}
}
#endif
