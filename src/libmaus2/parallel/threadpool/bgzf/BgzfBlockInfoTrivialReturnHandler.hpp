/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFBLOCKINFOTRIVIALRETURNHANDLER_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFBLOCKINFOTRIVIALRETURNHANDLER_HPP

#include <libmaus2/parallel/threadpool/bgzf/BgzfInputThreadControl.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct BgzfBlockInfoTrivialReturnHandler : BgzfBlockInfoHandlerInterface
				{
					ThreadPool & TP;
					BgzfInputThreadControl & BITC;

					BgzfBlockInfoTrivialReturnHandler(
						ThreadPool & rTP,
						BgzfInputThreadControl & rBITC
					)
					: TP(rTP), BITC(rBITC)
					{

					}

					virtual void registerBgzfBlock(uint64_t const, uint64_t const, uint64_t const)
					{

					}

					virtual void handleBgzfBlockInfo(libmaus2::util::shared_ptr<BgzfBlockInfo> ptr)
					{
						libmaus2::util::shared_ptr<BgzfBlockSummary> summary = ptr->owner;
						libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::input::ThreadPoolInputBlock> iblock = summary->ptr;
						libmaus2::parallel::threadpool::input::ThreadPoolInputInfo * inputinfo = summary->inputinfo;

						uint64_t const streamid = inputinfo->streamid;
						/* bool const blockfinished = */
							BITC.putInfo(streamid,ptr);

						if ( BITC.parseFinished() )
						{
							std::cerr << "[V] terminating" << std::endl;
							TP.terminate();
						}
					}
				};

			}
		}
	}
}
#endif
