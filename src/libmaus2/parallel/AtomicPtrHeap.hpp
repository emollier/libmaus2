/*
    libmaus2
    Copyright (C) 2021 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_ATOMICPTRHEAP_HPP)
#define LIBMAUS2_PARALLEL_ATOMICPTRHEAP_HPP

#include <libmaus2/parallel/AtomicPtrStack.hpp>

namespace libmaus2
{
	namespace parallel
	{
		template<typename type, typename comparator_type = std::less<type> >
		struct AtomicPtrHeap : private AtomicPtrStack<type>
		{
			private:
			std::shared_ptr<comparator_type> scomparator;
			comparator_type & comparator;

			public:
			AtomicPtrHeap()
			: scomparator(new comparator_type()), comparator(*scomparator)
			{
			}

			AtomicPtrHeap(comparator_type & rcomparator)
			: scomparator(), comparator(rcomparator)
			{

			}

			void push(libmaus2::util::shared_ptr<type> T)
			{
				AtomicPtrStack<type>::pushHeap(T,comparator);
			}

			bool pop(libmaus2::util::shared_ptr<type> & T)
			{
				return AtomicPtrStack<type>::popHeap(T,comparator);
			}

			std::size_t size()
			{
				return AtomicPtrStack<type>::size();
			}
		};
	}
}
#endif
