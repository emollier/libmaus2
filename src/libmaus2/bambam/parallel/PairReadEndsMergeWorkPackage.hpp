/*
    libmaus2
    Copyright (C) 2009-2015 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_BAMBAM_PARALLEL_PAIRREADENDSMERGEWORKPACKAGE_HPP)
#define LIBMAUS2_BAMBAM_PARALLEL_PAIRREADENDSMERGEWORKPACKAGE_HPP

#include <libmaus2/bambam/parallel/ReadEndsMergeRequest.hpp>
#include <libmaus2/parallel/SimpleThreadWorkPackage.hpp>
#include <libmaus2/bambam/ReadEndsBlockIndexSet.hpp>

namespace libmaus2
{
	namespace bambam
	{
		namespace parallel
		{
			struct PairReadEndsMergeWorkPackage : public libmaus2::parallel::SimpleThreadWorkPackage
			{
				typedef PairReadEndsMergeWorkPackage this_type;
				typedef std::unique_ptr<this_type> unique_ptr_type;
				typedef std::shared_ptr<this_type> shared_ptr_type;

				ReadEndsMergeRequest REQ;
				libmaus2::bambam::ReadEndsBlockIndexSetData::shared_ptr_type setData;
				libmaus2::parallel::StdSemaphore::shared_ptr_type psem;

				PairReadEndsMergeWorkPackage() : libmaus2::parallel::SimpleThreadWorkPackage(), REQ(), setData()
				{

				}
				PairReadEndsMergeWorkPackage(
					ReadEndsMergeRequest const & rREQ,
					libmaus2::bambam::ReadEndsBlockIndexSetData::shared_ptr_type rsetData,
					libmaus2::parallel::StdSemaphore::shared_ptr_type rpsem,
					uint64_t const rpriority,
					uint64_t const rdispatcherid,
					uint64_t const rpackageid = 0
				)
				: libmaus2::parallel::SimpleThreadWorkPackage(rpriority,rdispatcherid,rpackageid), REQ(rREQ), setData(rsetData), psem(rpsem)
				{

				}
				~PairReadEndsMergeWorkPackage() {}

				PairReadEndsMergeWorkPackage & operator=(PairReadEndsMergeWorkPackage const & O)
				{
					if ( this != &O )
					{
						libmaus2::parallel::SimpleThreadWorkPackage::operator=(O);
						REQ = O.REQ;
						setData = O.setData;
						psem = O.psem;
					}
					return *this;
				}

				char const * getPackageName() const
				{
					return "PairReadEndsMergeWorkPackage";
				}
			};
		}
	}
}
#endif
