/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_BAMBAM_BAMHEADERREADGROUPINTERFACE_HPP)
#define LIBMAUS2_BAMBAM_BAMHEADERREADGROUPINTERFACE_HPP

#include <libmaus2/bambam/ReadGroup.hpp>
#include <vector>

namespace libmaus2
{
	namespace bambam
	{
		struct BamHeaderReadGroupInterface
		{
			virtual uint64_t getNumReadGroups() const = 0;
			virtual int64_t getReadGroupId(char const * ID) const = 0;
			virtual std::string getReadGroupIdentifierAsString(int64_t const i) const = 0;
			virtual ~BamHeaderReadGroupInterface() {}
		};
	}
}
#endif
