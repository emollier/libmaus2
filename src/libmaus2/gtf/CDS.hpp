/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_CDS_HPP)
#define LIBMAUS2_GTF_CDS_HPP

#include <libmaus2/util/NumberSerialisation.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct CDS
		{
			uint64_t id;
			uint64_t start;
			uint64_t end;
			uint64_t strand;
			uint64_t frame;
			uint64_t exonid;

			std::string toString() const
			{
				std::ostringstream ostr;
				ostr << "CDS("
					<< id << ","
					<< start << ","
					<< end << ","
					<< strand << ","
					<< frame << ","
					<< exonid << ")";
				return ostr.str();
			}

			bool operator==(CDS const & C) const
			{
				bool const r =
					id == C.id
					&&
					start == C.start
					&&
					end == C.end
					&&
					strand == C.strand
					&&
					frame == C.frame
					&&
					exonid == C.exonid
					;

				if ( ! r )
					std::cerr << toString() << "!=" << C.toString() << std::endl;

				return r;
			}

			bool operator!=(CDS const & C) const
			{
				return !operator==(C);
			}

			CDS() {}
			CDS(
				uint64_t const rid,
				uint64_t const rstart,
				uint64_t const rend,
				bool const rstrand,
				uint64_t const rframe,
				uint64_t const rexonid
			) : id(rid), start(rstart), end(rend), strand(rstrand), frame(rframe), exonid(rexonid)
			{
			}

			std::ostream & serialise(std::ostream & out) const
			{
				out.write(reinterpret_cast<char const *>(this),sizeof(CDS));
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				in.read(reinterpret_cast<char *>(this),sizeof(CDS));
				assert ( in.gcount() == sizeof(CDS) );
				return in;
			}

			int64_t getFrom() const
			{
				return static_cast<int64_t>(start)-1;
			}

			int64_t getTo() const
			{
				return end;
			}
		};

		std::ostream & operator<<(std::ostream & out, CDS const & C);
	}
}
#endif
