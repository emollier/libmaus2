/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_EXONINFO_HPP)
#define LIBMAUS2_GTF_EXONINFO_HPP

#include <ostream>
#include <cassert>
#include <libmaus2/gtf/ExonSubInfo.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct ExonInfo
		{
			libmaus2::gtf::ExonSubInfo unique;
			libmaus2::gtf::ExonSubInfo ambig;

			ExonInfo() : unique(), ambig() {}

			ExonInfo & operator+=(ExonInfo const & E)
			{
				unique += E.unique;
				ambig += E.ambig;
				return *this;
			}

			uint64_t getTotal() const
			{
				return unique.total + ambig.total;
			}

			double getUncoveredFraction() const
			{
				uint64_t const u = unique.uncovered + ambig.uncovered;
				uint64_t const t = unique.total + ambig.total;

				if ( t )
				{
					double const dt = t;
					return u / dt;
				}
				else
				{
					return 1.0;
				}
			}
		};

		std::ostream & operator<<(std::ostream & out, ExonInfo const & E);
	}
}
#endif
