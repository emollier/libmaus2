/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_TAG_HPP)
#define LIBMAUS2_GTF_TAG_HPP

#include <libmaus2/util/NumberSerialisation.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct Tag
		{
			uint64_t text_offset;

			std::string toString() const
			{
				std::ostringstream ostr;
				ostr << "Tag(" << text_offset << ")";
				return ostr.str();
			}

			bool operator==(Tag const & C) const
			{
				bool const r = text_offset == C.text_offset;

				if ( ! r )
					std::cerr << toString() << "!=" << C.toString() << std::endl;

				return r;
			}

			bool operator!=(Tag const & C) const
			{
				return !operator==(C);
			}

			Tag() {}
			Tag(
				uint64_t const rtext_offset
			) : text_offset(rtext_offset)
			{
			}

			std::ostream & serialise(std::ostream & out) const
			{
				out.write(reinterpret_cast<char const *>(this),sizeof(Tag));
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				in.read(reinterpret_cast<char *>(this),sizeof(Tag));
				assert ( in.gcount() == sizeof(Tag) );
				return in;
			}
		};

		std::ostream & operator<<(std::ostream & out, Tag const & C);
	}
}
#endif
