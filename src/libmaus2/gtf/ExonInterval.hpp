/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_EXONINTERVAL_HPP)
#define LIBMAUS2_GTF_EXONINTERVAL_HPP

#include <libmaus2/util/NumberSerialisation.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct ExonInterval
		{
			uint64_t chr;
			uint64_t from;
			uint64_t to;

			ExonInterval()
			{
			}

			ExonInterval(uint64_t const rchr, uint64_t const rfrom, uint64_t const rto)
			: chr(rchr), from(rfrom), to(rto)
			{
			}

			bool operator<(ExonInterval const & RHS) const
			{
				if ( chr != RHS.chr )
					return chr < RHS.chr;
				else if ( from != RHS.from )
					return from < RHS.from;
				else
					return to > RHS.to;
			}

			std::ostream & serialise(std::ostream & out) const
			{
				out.write(reinterpret_cast<char const *>(this),sizeof(ExonInterval));
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				in.read(reinterpret_cast<char *>(this),sizeof(ExonInterval));
				return in;
			}
		};

		std::ostream & operator<<(std::ostream & out, ExonInterval const & U);
	}
}
#endif
