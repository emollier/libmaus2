/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/network/CurlInitObject.hpp>

#include <iostream>

#if defined(LIBMAUS2_HAVE_LIBCURL)
#include <curl/curl.h>
#endif

libmaus2::network::CurlInitObject::CurlInitObject()
{
	#if defined(LIBMAUS2_HAVE_LIBCURL)
	CURLcode code = curl_global_init(CURL_GLOBAL_ALL);

	if ( code != 0 )
	{
		std::cerr << "[E] curl_global_init() failed" << std::endl;
		std::abort();
	}
	#endif
}

libmaus2::network::CurlInitObject::~CurlInitObject()
{
	#if defined(LIBMAUS2_HAVE_LIBCURL)
	curl_global_cleanup();
	#endif
}
