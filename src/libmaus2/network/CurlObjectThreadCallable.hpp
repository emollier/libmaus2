/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined(LIBMAUS2_NETWORK_CURLOBJECTTHREADCALLABLE_HPP)
#define LIBMAUS2_NETWORK_CURLOBJECTTHREADCALLABLE_HPP

#include <libmaus2/network/CurlObject.hpp>
#include <libmaus2/parallel/StdThread.hpp>

namespace libmaus2
{
	namespace network
	{
		struct CurlObjectThreadCallable : public libmaus2::parallel::StdThreadCallable
		{
			typedef CurlObjectThreadCallable this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;

			libmaus2::network::CurlObject & ptr;
			std::atomic<int> code;
			std::string errs;

			CurlObjectThreadCallable(libmaus2::network::CurlObject & rptr);
			virtual ~CurlObjectThreadCallable();
			virtual void run();
			int getCode() const;
			long getFirstResponse() const;
			long getLastResponse() const;
			std::string getErrorString() const;
			std::string getHeaderData() const;
		};
	}
}
#endif
