/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined(LIBMAUS2_NETWORK_CURLINPUTSTREAMBUFFER_HPP)
#define LIBMAUS2_NETWORK_CURLINPUTSTREAMBUFFER_HPP

#include <libmaus2/network/CurlObjectReader.hpp>
#include <streambuf>
#include <istream>

namespace libmaus2
{
	namespace network
	{
		struct CurlInputStreamBuffer : public ::std::streambuf
		{
			typedef CurlInputStreamBuffer this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			private:
			libmaus2::network::CurlObjectReader COR;

			std::size_t const blocksize;
			std::size_t const putbackspace;

			::libmaus2::autoarray::AutoArray<char> buffer;

			CurlInputStreamBuffer(CurlInputStreamBuffer const &);
			CurlInputStreamBuffer & operator=(CurlInputStreamBuffer &);

			public:
			CurlInputStreamBuffer(std::string const & url, std::size_t const rblocksize = 16*1024ull, std::size_t const rputbackspace = 0);

			private:
			int_type underflow();
		};
	}
}
#endif
