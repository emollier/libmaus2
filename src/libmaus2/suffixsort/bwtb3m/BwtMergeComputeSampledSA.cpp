/**
    libmaus2
    Copyright (C) 2009-2021 German Tischler-Höhle
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <libmaus2/suffixsort/bwtb3m/BwtMergeComputeSampledSA.hpp>

template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::ByteInputTypes<32> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::ByteInputTypes<64> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::CompactInputTypes<32> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::CompactInputTypes<64> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::PacInputTypes<32> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::PacInputTypes<64> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::PacTermInputTypes<32> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::PacTermInputTypes<64> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::Lz4InputTypes<32> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::Lz4InputTypes<64> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::Utf8InputTypes<32> >;
template struct libmaus2::suffixsort::bwtb3m::BwtMergeComputeSampledSA< libmaus2::suffixsort::Utf8InputTypes<64> >;
