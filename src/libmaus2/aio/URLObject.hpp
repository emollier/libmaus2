/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_AIO_URLOBJECT_HPP)
#define LIBMAUS2_AIO_URLOBJECT_HPP

#include <string>
#include <regex>

namespace libmaus2
{
	namespace aio
	{
		struct URLObject
		{
			std::string protocol;
			std::string parameters;
			std::string path;

			URLObject(std::string const & s)
			{
				std::regex const r("^([a-z]+)(\\[[^\\]]*\\])?:(.*)$");
				std::smatch matches;

				if ( std::regex_search(s,matches,r) )
				{
					protocol = matches[1];
					parameters = matches[2];

					if ( parameters.size() )
					{
						assert ( parameters.front() == '[' );
						assert ( parameters.back()  == ']' );
						parameters = parameters.substr(1,parameters.size()-2);
					}

					path = matches[3];
				}
				else
				{
					path = s;
				}
			}
		};
	}
}
#endif
