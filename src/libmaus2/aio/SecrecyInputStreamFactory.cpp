/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/aio/SecrecyInputStreamFactory.hpp>

#if defined(LIBMAUS2_HAVE_LIBSECRECY)
#include <libmaus2/aio/InputStreamInstance.hpp>
#include <libmaus2/aio/SecrecyCache.hpp>
#include <libsecrecy/GCMInputStream.hpp>

namespace libmaus2
{
	namespace aio
	{
		struct SecrecyInputStreamInstanceWrapper
		{
			protected:
			InputStreamInstance ISI;

			public:
			SecrecyInputStreamInstanceWrapper(std::string const & fn) : ISI(fn) {}
		};
	}
}

namespace libmaus2
{
	namespace aio
	{
		struct SecrecyInputStream : public SecrecyInputStreamInstanceWrapper, public libsecrecy::GCMInputStream
		{
			static std::size_t getDefaultPutBackSize()
			{
				return 0;
			}

			SecrecyInputStream(
				std::string const & fn
			)
			: SecrecyInputStreamInstanceWrapper(fn),
			  libsecrecy::GCMInputStream(SecrecyInputStreamInstanceWrapper::ISI,getDefaultPutBackSize(),&SecrecyCache::secrecyCache)
			{}
		};
	}
}

libmaus2::aio::SecrecyInputStreamFactory::~SecrecyInputStreamFactory() {}

libmaus2::aio::InputStream::unique_ptr_type libmaus2::aio::SecrecyInputStreamFactory::constructUnique(std::string const & filename)
{
	std::shared_ptr<std::istream> iptr(new SecrecyInputStream(filename));
	libmaus2::aio::InputStream::unique_ptr_type istr(new libmaus2::aio::InputStream(iptr));
	return istr;
}

libmaus2::aio::InputStream::shared_ptr_type libmaus2::aio::SecrecyInputStreamFactory::constructShared(std::string const & filename)
{
	std::shared_ptr<std::istream> iptr(new SecrecyInputStream(filename));
	libmaus2::aio::InputStream::shared_ptr_type istr(new libmaus2::aio::InputStream(iptr));
	return istr;
}
#endif
