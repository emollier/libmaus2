/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/parallel/threadpool/ThreadPool.hpp>
#include <libmaus2/aio/StreamLock.hpp>

#include <libmaus2/parallel/AtomicStack.hpp>
#include <libmaus2/parallel/AtomicPtrStack.hpp>
#include <libmaus2/parallel/AtomicQueue.hpp>
#include <libmaus2/parallel/AtomicPtrQueue.hpp>
#include <libmaus2/parallel/AtomicPtrHeap.hpp>

struct HelloWorldThreadPackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
{
	std::string getText() const
	{
		return "Hello world";
	}
};

struct HelloWorldThreadPackageDispatcher : public libmaus2::parallel::threadpool::ThreadPoolDispatcher
{
	uint64_t const n;
	std::atomic<uint64_t> i;

	HelloWorldThreadPackageDispatcher(uint64_t const rn)
	: n(rn), i(0)
	{

	}

	virtual void dispatch(libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadWorkPackage> package, libmaus2::parallel::threadpool::ThreadPool * pool)
	{
		HelloWorldThreadPackageData & HP = dynamic_cast<HelloWorldThreadPackageData &>(*(package->data.load()));
		uint64_t const li = ++i;

		{
			libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(
				libmaus2::aio::StreamLock::cerrlock
			);
			std::cerr << "[" << li << "]\t" << HP.getText() << std::endl;
		}

		if ( li == n )
			pool->terminate();
	}
};

static void testAtomicPtrHeap()
{
	libmaus2::parallel::AtomicPtrHeap<int> APH;

	int n = 256;
	for ( int i = 0; i < n; ++i )
		APH.push(libmaus2::util::shared_ptr<int>(new int(n-i-1)));

	for ( int i = 0; i < n; ++i )
	{
		libmaus2::util::shared_ptr<int> ptr;
		bool const ok = APH.pop(ptr);
		assert ( ok );
		assert ( *ptr == i );
	}
}

static void testAtomicStack()
{
	libmaus2::parallel::AtomicStack<int> AS;

	int const n = 256;

	for ( int i = 0; i < n; ++i )
	{
		for ( int j = 0; j < i; ++j )
			AS.push(j);
		for ( int j = 0; j < i; ++j )
		{
			int r;
			bool const ok = AS.pop(r);
			assert ( ok );
			assert ( r == i-j-1 );
		}
	}
}

static void testAtomicPtrStack()
{
	libmaus2::parallel::AtomicPtrStack<int> AS;

	int const n = 256;

	for ( int i = 0; i < n; ++i )
	{
		for ( int j = 0; j < i; ++j )
		{
			libmaus2::util::shared_ptr<int> tptr(new int(j));
			AS.push(tptr);
		}
		for ( int j = 0; j < i; ++j )
		{
			libmaus2::util::shared_ptr<int> r;
			bool const ok = AS.pop(r);
			assert ( ok );
			assert ( *r == i-j-1 );
		}
	}
}

static void testAtomicQueue()
{
	libmaus2::parallel::AtomicQueue<int> AS;

	int const n = 256;

	for ( std::size_t z = 0; z < 3; ++z )
		for ( int i = 0; i < n; ++i )
		{
			for ( int j = 0; j < i; ++j )
				AS.push(j);
			for ( int j = 0; j < i; ++j )
			{
				int r;
				bool const ok = AS.pop(r);
				assert ( ok );
				assert ( r == j );
			}
		}
}

static void testAtomicPtrQueue()
{
	libmaus2::parallel::AtomicPtrQueue<int> AS;

	int const n = 256;

	for ( std::size_t z = 0; z < 3; ++z )
		for ( int i = 0; i < n; ++i )
		{
			for ( int j = 0; j < i; ++j )
			{
				libmaus2::util::shared_ptr<int> tptr(new int(j));
				AS.push(tptr);
			}
			for ( int j = 0; j < i; ++j )
			{
				libmaus2::util::shared_ptr<int> r;
				bool const ok = AS.pop(r);
				assert ( ok );
				assert ( *r == j );
			}
		}
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		testAtomicStack();
		testAtomicPtrStack();
		testAtomicQueue();
		testAtomicPtrQueue();
		testAtomicPtrHeap();

		libmaus2::parallel::threadpool::ThreadPool TP(12);
		uint64_t const n = 24;
		TP.registerDispatcher<HelloWorldThreadPackageData>(libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadPoolDispatcher>(new HelloWorldThreadPackageDispatcher(n)));
		for ( uint64_t i = 0; i < n; ++i )
		{
			uint64_t const prio = i;
			std::pair<uint64_t,uint64_t> Pid(i,0);
			libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadWorkPackage> pack(TP.getPackage<HelloWorldThreadPackageData>(prio,Pid));
			TP.enqueue(pack);
		}

		TP.join();
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
