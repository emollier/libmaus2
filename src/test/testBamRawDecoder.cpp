/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/bambam/BamRawDecoder.hpp>
#include <libmaus2/bambam/BamAlignmentDecoderBase.hpp>
#include <libmaus2/timing/RealTimeClock.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);
		std::string const infn = arg[0];
		libmaus2::bambam::BamRawDecoder BRD(infn);

		std::pair<
			std::pair<uint8_t const *,uint64_t>,
			libmaus2::bambam::BamRawDecoder::RawInterval
		> P;
		libmaus2::timing::RealTimeClock rtc;
		rtc.start();
		uint64_t c = 0;

		while ( (P = BRD.getPos()).first.first )
		{
			char const * name = libmaus2::bambam::BamAlignmentDecoderBase::getReadName(P.first.first);

			libmaus2::bambam::BamRawDecoder IBRD(
				infn,false,
				P.second.start
			);

			std::pair<
				std::pair<uint8_t const *,uint64_t>,
				libmaus2::bambam::BamRawDecoder::RawInterval
			> PP = IBRD.getPos();

			assert ( PP.first.first );
			assert ( PP.first.second == P.first.second );


			char const * PPname = libmaus2::bambam::BamAlignmentDecoderBase::getReadName(PP.first.first);
			assert ( strcmp(name,PPname) == 0 );
			assert ( PP.second == P.second );

			if ( ++c % (8*1024) == 0 )
			{
				double const sec = rtc.getElapsedSeconds();
				std::cerr << c << " "<< sec << " " << c / sec
					<< " " << name << " " << P.second.toString()
					<< std::endl;
			}
		}

		return 0;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
