/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/parallel/threadpool/bam/ThreadPoolBamParseTrivialReturnHandler.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/aio/StreamLock.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		libmaus2::parallel::threadpool::ThreadPool TP(12);
		libmaus2::parallel::threadpool::bam::ThreadPoolBamParseTrivialReturnHandler TPBPTRH;

		std::unique_ptr<libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl> pTPBDC;

		std::vector<std::string> Vfn;
		for ( uint64_t i = 0; i < arg.size(); ++i )
			Vfn.push_back(arg[i]);

		std::size_t const inputblocks = 64;
		std::size_t const inputblocksize = 256*1024;
		std::size_t const numdecompressedblocks = 256;

		if ( Vfn.size() )
		{

			std::unique_ptr<libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl>
				tptr(new libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl(TP,Vfn,&TPBPTRH,inputblocks,inputblocksize,numdecompressedblocks)
			);

			pTPBDC = std::move(tptr);
		}
		else
		{
			std::shared_ptr<std::istream> ptr(&std::cin, []([[maybe_unused]]auto p){});

			std::unique_ptr<libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl>
				tptr(new libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl(TP,std::vector< std::shared_ptr<std::istream> >(1,ptr),&TPBPTRH,inputblocks,inputblocksize,numdecompressedblocks)
			);

			pTPBDC = std::move(tptr);
		}

		pTPBDC->start();

		TP.join();

		std::cerr << "[V] finished and joined" << std::endl;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
